const User = require("../models/user");
const Product = require("../models/product");
const Order = require("../models/order");
const auth = require("../auth");
const bcrypt = require("bcrypt");


//CHECK OUT TRY***********************************************************
// module.exports.purchaseOrder = async (data) => {
// 	let newOrder = new Order ({
// 		userId: data.userId,
// 		orderDetails: data.orderDetails,
// 		totalAmount: data.totalAmount
// 	})

// 	return newOrder.save().then((order, error) => {
// 		if (error) {
// 			return false;
// 		}else{
// 			return true;
// 		}
// 	})

// 	// console.log(data)
// }


//CHECKOUT*****************************************************************
// module.exports.purchaseOrder = async (req, res) => {
// 	try {
// 		const userId = auth.decode(req.headers.authorization).id
// 		const orderDetails = req.body.orderDetails
// 		const totalAmount = req.body.totalAmount

// 		const newOrder = new Order ({
// 		userId: userId,
// 		orderDetails: orderDetails,
// 		totalAmount: totalAmount
// 		})

// 		await newOrder.save();

// 		res.status(200).send("Order is saved")

// 	} catch	(error) {
// 		res.status(500).send("There's an error")
// 		console.log(error)
// 	}
// }

module.exports.purchaseOrder = (data) => {
	let newOrder = new Order({
		userId: data.userId,
		orderDetails: data.orderDetails,
		totalAmount: data.totalAmount
	});

	return newOrder.save().then((order, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	})	

}



//PURCHASES PER USER********************************************************
module.exports.userPurchases = (data) => {
	return Order.find({userId: data}).then(result => {
		return result;
	})
}




//ALL ORDERS ADMIN***********************************************************
module.exports.getAllOrders = (body) => {
	return Order.find(body).then(result => {return result
	})
}