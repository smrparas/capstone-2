//import model
const Product = require("../models/product");


// ADD A NEW PRODUCT*******************************************
module.exports.addProduct = (body) => {
	
	let newProduct = new Product({
		name: body.name,
		description: body.description,
		price: body.price,
		category: body.category,
		stock: body.stock
	})

	return newProduct.save().then((product, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	})
}


//GET ALL ACTIVE PRODUCTS***************************************
module.exports.getAllActive = (req, res) => {
	return Product.find().then(result => {
		return result;
	})
}


//GET SPECIFIC PRODUCT******************************************
module.exports.getProduct = (params) => {
	return Product.findById(params.productId).then(result => {
		return result;
	})
}


//UPDATE A PRODUCT*********************************************
module.exports.updateProduct = (params, body) => {
	let updatedProduct = {
		name: body.name,
		description: body.description,
		price: body.price,
		category: body.category,
		stock: body.stock
	}

	return Product.findByIdAndUpdate(params.productId, updatedProduct).then((product, error) => {
		if (error) {
			return false;
		}else{
			return true;
		}
	})
}



//ARCHIVE PRODUCT******************************************************
module.exports.archive = (params, body) => {

	let updatedProduct;

	if(body.isActive)
	{
		updatedProduct = {

			isActive : false

		}
	}
	else
	{
		updatedProduct = {

			isActive : true
		}
	}

	return Product.findByIdAndUpdate(params.productId, updatedProduct).then((product, error) => {

		if(error)
		{
			return false;
		}
		else
		{
			return true;
		}

	})
}


//Enable PRODUCT******************************************************
module.exports.enable = (params) => {
	let updatedProduct = {
		isActive: true
	}
	
	return Product.findByIdAndUpdate(params.productId, updatedProduct).then((product, error) => {
		if (error) {
			return false;
		}else{
			return true;
		}
	})
}


//TRIAL ONLY***********************************************************
//GET PURCHASES (ADMIN)
module.exports.adminView = (req, res) => {
	return Product.find({purchases}).then(result => {
		return result;
	})
}