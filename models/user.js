const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	username: {
		type: String,
		required: [true, "username is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile no. is required"]
	},
	address: {
		type: String,
		required: [true, "Address is required"]
	},
	// birthday: {
	// 	type: Date,
	// 	required: [true, "Birthday is required"],
	// 	trim: true
	// },
	gender: {
		type: String,
		required: [true, "Gender is required"]
	},
	birthday: [ 
		{
			day: {
			  type: Number,
			  required: true
			},
			month: {
			  type: Number,
			  required: true
			},
			year: {
			  type: Number,
			  required: true
			}
		}
	],
	// purchases: [
	// 	{
	// 		productId: {
	// 			type: String,
	// 			required: [true, "Name is required"]
	// 		},
	// 		purchasedOn: {
	// 			type: Date,
	// 			default: new Date()
	// 		},
	// 		quantity: {
	// 			type: Number,
	// 			required: [true, "Quantity is required"]
	// 		},
	// 		totalAmount: {
	// 			type: Number,
	// 			required: [true, "Total amount is required"]
	// 		}
	// 	}
	// ]
})

module.exports = mongoose.model("User", userSchema);
