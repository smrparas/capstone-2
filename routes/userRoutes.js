const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/userController");


//CHECK EXISTING/DUPLICATE EMAIL**************************
router.post("/checkEmail", (req, res) => {
	userController.checkEmail(req.body).then(resultFromEmailExists => res.send(resultFromEmailExists))
})


//REGISTRATION/ CREATE NEW USER***************************
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromRegister => res.send(resultFromRegister))
})


//LOG-IN***************************************************
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromLoginUser => res.send(resultFromLoginUser))
})


//USER DETAILS*********************************************
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	userController.getProfile(userData.id).then(resultFromGetProfile => res.send(resultFromGetProfile))
})




//**************************************TRIAL ONLY******************************************

//CHECKOUT ORDER*********************************************
router.post("/checkout", auth.verify, (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity,
		totalAmount: req.body.totalAmount
	}

	userController.checkout(data).then(resultCheckout => res.send(resultCheckout))
})


//GET PURCHASES (per user)************************************
router.get("/purchases", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.getPurchases(userData.id).then(resultPurchases => res.send(resultPurchases))
})


//CHANGE TO ADMIN********************************************
router.put("/:userId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if (token.isAdmin === true) {
		userController.changeToAdmin(req.params, req.body).then(result => res.send(result))
		}else{
			res.send({auth: "Update is for admin only"})
	}
})


module.exports = router