const express = require("express");
const router = express.Router();
const auth = require("../auth");
const orderController = require("../controllers/orderController");

//CHECKOUT TRY************************************************************
router.post("/", auth.verify, (req, res) => {
	console.log(req.body)
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		orderDetails: req.body.orderDetails,
		totalAmount: req.body.totalAmount,
	}

	orderController.purchaseOrder(data).then(resultFromController => res.send(resultFromController));


	// .then(result => res.send(result))
	// console.log(orderController)
})


//CHECKOUT****************************************************************
// router.post("/", auth.verify)



//GET PURCHASES PER USER**************************************************
router.get("/myPurchases", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	orderController.userPurchases(userData.id).then(result => res.send(result))
})




//ALL ORDERS - ADMIN*****************************************************
router.get("/allOrders", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if (token.isAdmin === true) {
		orderController.getAllOrders().then(result => res.send(result))
	}else{
		res.send({auth: "You're not an admin"})
	}
})



module.exports = router